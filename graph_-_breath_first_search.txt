edges = [[1,2],[2,3],[1,3],[1,7],[5,6],[6,8],[5,8],[8,9],[4,7],[2,5]]
### depth uses stack, breath uses queue
### depth explores maximum paths over neighbors, breath explores closest neighbors first

def create_graph(edges):
    graph = {}
    for [a,b] in edges:
        if a not in graph.keys():
            graph[a] = []
        if b not in graph.keys():
            graph[b] = []
        graph[a].append(b)
        graph[b].append(a)
    return graph
    
def breath_first_search(graph, start, target, visited):
    stack = [start]
    visited.add(start)
    while stack:
        current = stack[0]
        stack = stack[1:]
        for neighbor in graph[current]:
            if neighbor == target:
                return True
            if neighbor not in visited:
                stack.append(neighbor)
                visited.add(neighbor)
        print(stack, current)
    return False

graph = create_graph(edges)
print(graph)
start = 1
target = 4
visited = set()
breath_first_search(graph, start, target, visited)   